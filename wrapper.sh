#!/bin/bash

# Check command format
if [ $# -ne 5 ]
then
		echo "./wrapper.sh <source-srt-file> <source-video-file> <tts-audio-list-file> <working-folder> <skip-srt>(0/1 - skips the first srt entry)"
fi

# Read arguments
source_srt_file=$1
source_video_file=$2
tts_audio_list_file=$3
working_folder=$4
skip_flag=$5

source_srt_file=input_data/source.srt
source_video_file=input_data/source.mp4
tts_audio_list_file=input_data/tts_audio_list.txt
working_folder=~/S2S_Project/LipSyncV2
skip_flag=1

# Check skip-flag
if [ $skip_flag == 0 ] || [ $skip_flag == 1 ]
then
		echo "skip-flag set"
else
		echo "skip-flag not set properly"
		exit 0
fi

# Define variables
temp_data_folder=$working_folder/temp_data
out_vid_seg_loc=$working_folder/temp_data/src_video_segs
out_aud_seg_loc=$working_folder/temp_data/src_audio_segs
src_boundary_files=$working_folder/temp_data/src_boundary_files
tts_boundary_files=$working_folder/temp_data/tts_boundary_files
tts_sil_rem_segs=$working_folder/temp_data/tts_sil_rem_segs

vid_list_file=$working_folder/temp_list/src_video_segs_list.txt
video_list_file=$working_folder/temp_list/src_video_nosound_segs_list.txt
aud_list_file=$working_folder/temp_list/src_audio_sound_list.txt
tts_aud_list_file=$working_folder/temp_list/tts_audio_sil_rem_list.txt
full_vid_list_file=$working_folder/temp_list/srt_full_video_list.txt
align_vid=$working_folder/realign_videos
final_vid=$working_folder/final_videos

# Create new temp-folders
rm -rf $temp_data_folder
rm -rf $align_vid
rm -rf $final_vid
rm -rf $working_folder/temp_list

mkdir $temp_data_folder
mkdir $out_aud_seg_loc
mkdir $out_vid_seg_loc
mkdir $src_boundary_files
mkdir $tts_boundary_files
mkdir $tts_sil_rem_segs
mkdir $align_vid
mkdir $final_vid
mkdir $working_folder/temp_list

# Remove audio from video
file_name=${source_video_file##*/}
file_name=${file_name%.*}
source_video_nosound_file=$working_folder/temp_data/${file_name}_nosound.mp4
ffmpeg -i $source_video_file -c copy -an $source_video_nosound_file # source video without sound

# Split source video and check if the numbers match with number of tts audio files
python scripts/splitSrc.py -v $source_video_file -n $source_video_nosound_file -s $source_srt_file -t $tts_audio_list_file -o $vid_list_file -f $full_vid_list_file -l $out_vid_seg_loc -k $skip_flag

# Source video segments without audio
less $vid_list_file | parallel -k "ffmpeg -i {1} -c copy -an {1.}_nosound.mp4"
less $vid_list_file | parallel -k -j1 "echo {1.}_nosound.mp4 >> $video_list_file"

# Source audio segs from video segs
less $vid_list_file | parallel "ffmpeg -i {1} -ac 1 $out_aud_seg_loc/{1/.}.wav"
sed "s:src_video_segs:src_audio_segs:g" $vid_list_file | sed "s:.mp4:.wav:g" > $aud_list_file 

# Remove sil from begin and end of tts audio
less $tts_audio_list_file | parallel "sox --norm {1} $tts_sil_rem_segs/{1/.}_sil_rem.wav silence 1 0.1 1% reverse silence 1 0.1 1% reverse"
#less $tts_audio_list_file | parallel "sox --norm {1} $tts_sil_rem_segs/{1/.}_sil_rem.wav silence 1 0.1 0.5% reverse silence 1 0.1 0.5% reverse"
#less $tts_audio_list_file | parallel "sox --norm {1} $tts_sil_rem_segs/{1/.}_norm.wav"
#less $tts_audio_list_file | parallel "python scripts/remove_silences.py -i $tts_sil_rem_segs/{1/.}_norm.wav -o $tts_sil_rem_segs/{1/.}_sil_rem.wav"
#sed "s:input_data/tts_audio_segs:temp_data/tts_sil_rem_segs:g" $tts_audio_list_file | sed "s:.wav:_sil_rem.wav:g" > $tts_aud_list_file
less $tts_audio_list_file | parallel -k -j1 "echo $tts_sil_rem_segs/{1/.}_sil_rem.wav" > $tts_aud_list_file

# Extract boundaries for both src and tts audio
less $tts_aud_list_file | parallel "ffmpeg -y -i {1} -f wav -flags +bitexact -acodec pcm_s16le -ar 22050 -ac 1 $tts_sil_rem_segs/{1/.}_clean_wav.wav; bin/LongSilence bin/config/fe-words.base-tts $tts_sil_rem_segs/{1/.}_clean_wav.wav $tts_boundary_files/{1/.}.temp $tts_boundary_files/{1/.}_boundary_file.txt 50 10"
#less $tts_audio_list_file | parallel -k -j1 "echo $tts_sil_rem_segs/{1/.}_sil_rem_clean_wav.wav" > $tts_aud_list_file
less $tts_aud_list_file | parallel -k -j1 "echo $tts_boundary_files/{1/.}_boundary_file.txt >> $working_folder/temp_list/tts_boundary_list.txt"

less $aud_list_file | parallel "ffmpeg -y -i {1} -f wav -flags +bitexact -acodec pcm_s16le -ar 44100 -ac 1 $out_aud_seg_loc/{1/.}_clean_wav.wav; bin/LongSilence bin/config/fe-words.base-src $out_aud_seg_loc/{1/.}_clean_wav.wav $src_boundary_files/{1/.}.temp $src_boundary_files/{1/.}_boundary_file.txt 80 40"
less $aud_list_file | parallel -k -j1 "echo $src_boundary_files/{1/.}_boundary_file.txt >> $working_folder/temp_list/src_boundary_list.txt"

less $vid_list_file | parallel -k -j1 "echo {1/.} >> $working_folder/temp_list/filename.txt"

# Read boundary files, align and create output video
paste <(paste $working_folder/temp_list/src_boundary_list.txt $working_folder/temp_list/tts_boundary_list.txt $video_list_file) <(paste <(sed "s:$:\t$working_folder/temp_list/output_align_video_list.txt:g" $tts_aud_list_file | sed "s:$:\t$align_vid:g") $working_folder/temp_list/filename.txt) > $working_folder/temp_list/align_seg_list.txt

# Align segment tts and src segment
less $working_folder/temp_list/align_seg_list.txt | parallel -k --bar --colsep '\t' "python scripts/align-seg.py -s {1} -t {2} -v {3} -a {4} -l {5} -d {6} -f {7}"

paste <(paste -d'/' $working_folder/temp_list/output_align_video_list.txt <(rev $working_folder/temp_list/output_align_video_list.txt | cut -d'/' -f1 | rev) | sed "s:$:_list.txt:g") <(rev $working_folder/temp_list/output_align_video_list.txt | cut -d'/' -f1 | rev) > $working_folder/temp_list/merge_video_list.txt

# Merge intra segment video segments
#less $working_folder/temp_list/merge_video_list.txt | parallel --colsep '\t' "ffmpeg -f concat -i {1} -c copy -fflags +genpts $final_vid/{2/.}.mp4"

cp $out_vid_seg_loc/intersrt_seg* $final_vid

rev $full_vid_list_file | cut -d'/' -f1 | rev | sed "s:^:file ':g" | sed "s:$:':g" > $final_vid/final_merge_list.txt

# Merge segments to final output video
#ffmpeg -f concat -i $final_vid/final_merge_list.txt -c copy -fflags +genpts output_video.mp4
ffmpeg -f concat -i $final_vid/final_merge_list.txt -fflags +genpts output_video.mp4

