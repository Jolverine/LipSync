## LipSync-ing script

#### Lip syncing code given source video, source srt and TTS outputs in different languages.

*Author: Jom Kuriakose*\
*email: jom@cse.iitm.ac.in*\
*Date: 17/01/2021*

###### Running wrapper script - Example

```bash
./wrapper.sh input_data/source.srt input_data/source.mp4 input_data/tts_audio_list.txt ~/S2S_Project/LipSyncV2 1
```

###### Command format
```bash
./wrapper.sh <source-srt-file> <source-video-file> <tts-audio-list-file> <working-folder> <skip-srt>(0/1 - skips the first srt entry)
```




